#!/bin/bash

set -e
set -o pipefail

# set locale specifically, to get consistent sort order
export LC_ALL=C

swift_version=$(dpkg -s swift | sed -ne 's/Version: //p')

#Work out which python to use - if we're on bullseye's swift then use
#python3; use py2 on a lesser version

if dpkg --compare-versions "$swift_version" lt 2.26.0-10 ; then
    PYTHON=python
else
    PYTHON=python3
fi

runtest()
{
    if [ "$#" -ne 2 ]; then
        echo " Internal Error: runtest requires two arguments"
        exit 1
    fi
    dir="$1"
    args=(-v -c "${dir}/hosts.yaml" --ring-dir "$dir" \
	     --skip-dispersion-check --host-file-path "${dir}/testing_hosts" \
	     --skip-replication-check)
    if [ "$2" = "immediate" ]; then
	compare="immediate_output.txt"
	args+=('--immediate-only')
    else
	compare="full_output.txt"
    fi
    outf=$(mktemp)
    trap 'rm -f $outf' EXIT
    set +e
    $PYTHON swift_ring_manager.py "${args[@]}" | sort >"$outf"
    rc="$?"
    if [ "$rc" -ne 0 ]; then
	echo " swift_ring_manager failed; output was:"
	cat "$outf"
	exit 1
    fi
    set -e
    if ! diff -q "${dir}/${compare}" "$outf" >/dev/null ; then
	echo " Output has changed. diff follows:"
	diff -u "${dir}/${compare}" "$outf"
	#diff returns non-zero, so script exits
    fi
    rm "$outf"
}

runtests()
{
    if [ "$#" -ne 1 ]; then
        echo " Internal Error: runtests requires a single argument"
        exit 1
    fi
    dir="$1"
    runtest "$dir" ""
    runtest "$dir" "immediate"
}

#More robust than for dir in $(find...)
#cf https://www.shellcheck.net/wiki/SC2044
while IFS= read -r -d '' dir
do
    echo -n "Testing $dir..."
    runtests "$dir"
    echo "passed"
done < <(find tests -mindepth 1 -maxdepth 1 -type d -print0)
