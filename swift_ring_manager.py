#!/usr/bin/env python
"""Swift ring manager

This is really intended to be run as a CLI tool (pass -h for a usage
message), but the individual functions may be useful for development
or debugging.

"""
# pylint: disable=C0103,C0302,W0603

from __future__ import print_function

import argparse
import atexit
import collections
import fnmatch
import ipaddress
import json
import logging
import logging.handlers
import os
import os.path
import shutil
import socket
import subprocess
import sys
import tarfile
import tempfile
import time
import yaml

from six.moves.urllib.parse import urlparse
import six

from swift.cli.recon import SwiftRecon, Scout
from swift.common import exceptions
from swift.common.ring import RingBuilder
from swift.common.storage_policy import POLICIES
from swift.common.utils import lock_path

logger = logging.getLogger()

#Proportion of full weight we move in a single step
gradual = 0.04

#Caches of forward and reverse DNS
iptohn_cache = {}
hntoip_cache = {}

#temporary directory for editing ring files in
tmpdir = None

#object for storing desired state, with two parameters:
#weight (number, may be None); and
#immediate (boolean), indicating if any necessary change should be
#made in one fell swoop rather than gradually.
WeightChange = collections.namedtuple('WeightChange','weight,immediate')

def reverse_dns(ip, check_forwards=True):
    '''reverse_dns(ip)->short_hostname caching reverse DNS lookup

    returns the short hostname corresponding to the IP address provided.

    If check_forwards is True (the default) and the IP wasn't in our
    cache, check that the hostname found resolves back to our IP.

    '''
    global iptohn_cache
    if ip not in iptohn_cache:
        fqdn = socket.gethostbyaddr(ip)[0]
        iptohn_cache[ip] = fqdn.split('.')[0]
        if check_forwards:
            checked_ip = forward_dns(iptohn_cache[ip],
                                     check_reverse=False)
            if ip != checked_ip:
                raise ValueError("DNS inconsistent: {}->{}->{}".format(
                    ip, iptohn_cache[ip], checked_ip))
    return iptohn_cache[ip]

def forward_dns(hostname, check_reverse=True):
    '''forward_dns(hostname)->IP caching DNS lookup

    returns the IP address corresponding to the hostname provided

    If check_reverse is True (the default) and the hostname wasn't in
    our cache, check that the IP found resolves back to our hostname.

    '''
    global hntoip_cache
    if hostname not in hntoip_cache:
        ans = socket.getaddrinfo(hostname, None, 0, 0,
                                 socket.IPPROTO_TCP,
                                 socket.AI_CANONNAME)
        #return is list of (family, socktype, proto, canonname, sockaddr)
        for a in ans:
            if a[0] == socket.AF_INET: #v4, use AF_INET6 to move to v6
                hntoip_cache[hostname] = a[4][0]
                if check_reverse:
                    checked_hostname = reverse_dns(a[4][0],
                                                   check_forwards=False)
                    if hostname != checked_hostname:
                        raise ValueError("DNS inconsistent: {}->{}->{}".format(
                            hostname, a[4][0], checked_hostname))
    return hntoip_cache[hostname]

def write_host_file(config,path):
    '''write_host_file(config,path) -> DNS details of hosts in config to path

    config is the desired_state from parsing our config file,
    relevantly it is a dictionary keyed on hostname.

    Take all the hosts from this, look up their IP and write the
    result to path in lines of the form:
    hostname IP

    The resulting file can be used for testing on a host without suitable
    DNS - e.g. an external development machine

    '''
    with open(path, "w") as f:
        print("# Fake hosts file for testing swift_ring_manager",
              file=f)
        for host in config:
            ip = forward_dns(host)
            print("{} {}".format(host,ip), file=f)

def read_host_file(path):
    '''read_host_file(path) -> load host<->IP mappings from path

    Populates the DNS caches so we can test swift_ring_manager from a
    host without access to WMF internal DNS, or to simulate extra
    hosts.

    '''
    global iptohn_cache, hntoip_cache
    with open(path, "r") as f:
        for line in f:
            #comment lines
            if line[0] == '#':
                continue
            host, ip = line.split()
            iptohn_cache[ip] = host
            hntoip_cache[host] = ip

def debug(s):
    '''debug() - print s iff verbose logging specified'''
    logging.debug(s)

def check_all_dispersions():
    '''check_all_dispersions - check dispersion for all policies'''
    for p in POLICIES:
        if not check_dispersion(p.name):
            return False
    return True

def check_dispersion(policyname):
    '''check_dispersion - check container & objects fully dispersed'''
    debug("Checking dispersion for policy {}".format(policyname))
    dro,dre = run_and_check(["/usr/bin/swift-dispersion-report",
                             "-j","-P",policyname])
    #The stretch version has a bug where it outputs "Using storage policy..."
    #before the JSON :(
    if dro.count(b'\n') == 2:
        dro = dro.split(b'\n')[1]
    drj = json.loads(dro)
    opf = drj["object"]["pct_found"]
    cpf = drj["container"]["pct_found"]
    if opf < 100.0 or cpf < 100.0:
        debug("{} dispersion incomplete - object {}, container {}".format(
            policyname, opf, cpf))
        if len(dre) != 0:
            debug("swift-dispersion-report stderr: {}".format(
                dre.decode(errors="replace")))
        return False
    return True

def check_replication():
    '''check_replication - check replication completions are close

    We want oldest and newest completion to be within an hour of each
    other (this is the best proxy for all ongoing rebalancing to be
    complete). An hour is a heuristic figure.

    This is a pared-down version of swift.cli.recon.replication_check
    '''
    # pylint: disable=W0212,W0612
    least_recent_time = 9999999999
    least_recent_url = None
    most_recent_time = 0
    most_recent_url = None
    reconnoiter = SwiftRecon()
    ring_names = reconnoiter._get_ring_names()
    hosts = reconnoiter.get_hosts(None,None,"/etc/swift",ring_names)
    recon = Scout("replication/%s" % reconnoiter.server_type)
    for url, response, status, ts_start, ts_end in reconnoiter.pool.imap(
            recon.scout, hosts):
        if status == 200:
            last = response.get('replication_last',
                                response.get('object_replication_last', 0))
            if last is None:
                continue
            if last < least_recent_time:
                least_recent_time = last
                least_recent_url = url
            if last > most_recent_time:
                most_recent_time = last
                most_recent_url = url
    if least_recent_url is None or most_recent_url is None:
        debug("Null URL in replication check ({} , {})".format(least_recent_url,
                                                               most_recent_url))
        return False
    diff = most_recent_time - least_recent_time
    if diff > 3600: #an hour
        debug("Replication skew ({}s) too high:".format(diff))
        debug("oldest host {} at {}".format(urlparse(least_recent_url).netloc,
                                            time.asctime(time.gmtime(least_recent_time))))
        return False
    return True

def load_rings(swiftdir="/etc/swift"):
    '''load_rings loads rings and converts to a host-key'd dict or None

    Exit if we shouldn't proceed for some reason
    (e.g. it's too soon since the previous rebalance)

    Otherwise the structure is:
    d["hostname"]={"devicename":{"ring":weight}}
    '''
    d = {}
    for f in os.listdir(swiftdir):
        if f.endswith(".builder"):
            d = load_ring("{}/{}".format(swiftdir,f),d)
            if d is None:
                sys.exit()
    return d

def load_ring(path,d):
    '''load_ring(path,d) -> load ring builder at path, add entries to d

    returns d, or None if we shouldn't proceed for some reason
    '''
    #/etc/swift/object.builder -> object
    ringname = os.path.splitext(os.path.basename(path))[0]
    builder = RingBuilder.load(path)
    #Check for conditions that mean we shouldn't change anything
    if builder.min_part_seconds_left > 0:
        debug("Need to wait {} seconds before changing ring {}".format(
            builder.min_part_seconds_left,path))
        return None
    #This check only available in newer swifts
    try:
        if builder.next_part_power:
            debug("partition power increase in progress in {}".format(path))
            return None
    except AttributeError:
        pass
    for dev in builder.devs:
        if dev is None:
            #not all ring ids are assigned necessarily
            continue
        hostname = reverse_dns(dev['ip'])
        if hostname not in d:
            d[hostname] = {}
        if dev['device'] not in d[hostname]:
            d[hostname][dev['device']] = {}
        d[hostname][dev['device']][ringname] = dev['weight']
    return d

def parse_device_overrides(conf):
    '''parse_device_overrides(conf) -> WeightChange

    parse a device override configuration, which might be:
    int/float: weight specified
    string: drain, failed, or immediate
    list: setting both immediate and weight explicitly
    If unspecified, immediate defaults to False
    weight need not be set

    Return a corresponding WeightChange
    '''
    #simple case first, just weight specified
    if isinstance(conf, (int,float) ):
        return WeightChange(conf, False)
    if isinstance(conf, str):
        if conf == "drain":
            return WeightChange(0, False)
        if conf == "failed":
            return WeightChange(0, True)
        if conf == "immediate":
            return WeightChange(None, True)
        raise ValueError("Invalid override value: {}".format(conf))
    #Finally the case where a list is specified that should contain
    #both a weight and an immediate specification.
    #Also deal with the user only specifying one but still in a list
    weight = None
    immediate = None
    for val in conf:
        if isinstance(val, (int,float) ):
            if weight is not None:
                raise ValueError("Only one weight per device allowed")
            weight = val
        elif isinstance(val, str):
            if immediate is not None:
                raise ValueError("Only one immediacy value per device allowed")
            if val not in ["immediate","drain","failed"]:
                raise ValueError("Invalid immediacy value {}".format(val))
            immediate = val
        else:
            raise ValueError("Invalid device override {}".format(val))
    #Now check they've specified something sensible
    if immediate in ["drain", "failed"]:
        if weight is not None:
            raise ValueError("{} may not be specified with a weight".format(immediate))
        if immediate == "drain":
            return WeightChange(0, False)
        if immediate == "failed":
            return WeightChange(0, True)
    if immediate == "immediate":
        return WeightChange(weight, True)
    #No immediacy value set, just weight (so default to False)
    if weight is None:
        raise ValueError("No immediacy or weight set in {}".format(conf))
    return WeightChange(weight, False)

def parse_host_overrides(conf):
    '''parse_host_overrides(conf) -> (hostname,overrides|None)

    parse a host configuration, including any overrides specified at
    the host or device level. Returns (hostname,overrides) where
    hostname is the short hostname, and overrides is a list of
    overrides or None.

    Overrides is a dictionary with keys of devicename (or "host" for
    host-level overrides).

    host-level overrides are a single string, one of "drain",
    "failed", or "immediate".

    device-level overrides are a tuple, the first member of which is a
    Boolean indicating whether this is an immediate change, the second
    optional value is a weight.
    '''
    if isinstance(conf,str): #straightforward, no overrides
        return (conf, None)
    if isinstance(conf,dict): #overrides specified pylint: disable=R1705
        if len(conf) != 1: #should be a single key, the hostname
            raise ValueError("Invalid host spec, parsed as {}".format(conf))
        hostname = list(conf)[0]
        overrides = {}
        #Overrides are specified as a YAML list entries being either:
        #a string - host-level override, or
        #a dictionary - device override (device is the dict key)
        #that might be a simple value (weight or immediate) or
        #a list containing both weight and immediate
        #or None (i.e. it was left as hostname: with no override defined)
        if conf[hostname] is None:
            return (hostname, None)
        ol = conf[hostname]
        for o in ol:
            if isinstance(o,str): #host-level
                if 'host' in overrides:
                    raise ValueError("Only 1 host-level override allowed")
                if o not in ['drain','failed','immediate']:
                    raise ValueError("Invalid host override: {}".format(o))
                overrides['host'] = o
                continue #done with this override
            #Handle device overrides, which are dicts
            for device in o:
                if device in overrides:
                    raise ValueError("device {} specified more than once".format(device))
                overrides[device] = parse_device_overrides(o[device])
        return(hostname, overrides)
    else:
        raise ValueError("Invalid host/override spec: {}".format(conf))

def apply_scheme(overrides,scheme):
    '''apply_scheme(overrides,scheme) -> dict

    Combine the scheme and overrides to calculate weights (and
    immediacy) for all devices.

    Return is a dictionary keyed on device, each entry of which is a
    WeightChange object.

    '''
    ret = {}
    #Check for host-level overrides first
    hweight = None
    himmediate = False
    if overrides is not None and 'host' in overrides:
        if overrides['host'] == "drain" or overrides['host'] == "failed":
            hweight = 0
        if overrides['host'] == "failed" or overrides['host'] == "immediate":
            himmediate = True
    weights = scheme["weight"]
    #Now for each scheme, apply weight to each device, including overrides
    for ring in scheme:
        #"weight" is the array of weights, not a ring
        if ring == "weight":
            continue
        if ring not in weights:
            raise ValueError("Weights for {} undefined".format(ring))
        for dev in scheme[ring]:
            immediate = himmediate
            weight = weights[ring]
            if hweight is not None:
                weight = hweight
            if overrides is not None and dev in overrides:
                #we want immediate if _either_ host or device sets it
                immediate |= overrides[dev].immediate
                if overrides[dev].weight is not None:
                    weight = overrides[dev].weight
            ret[dev] = WeightChange(weight, immediate)
    return ret

def parse_scheme_hosts(hosts,scheme,ret):

    '''parse_scheme_hosts(hosts,scheme,ret) -> updated ret

    return is a dictionary keyed on short hostname; update entries in
    it for hosts; apply scheme to their config including any host or
    device overrides specified.

    '''
    for h in hosts:
        hostname,overrides = parse_host_overrides(h)
        if hostname in ret:
            raise ValueError("Host {} defined more than once".format(hostname))
        ret[hostname] = apply_scheme(overrides,scheme)
    return ret

def build_device_weight_map(scheme):
    '''build_device_weight_map(scheme) -> dictionary

    Return is a dictionary d[ring][device]: weight
    '''
    d = {}
    weights = scheme["weight"]
    for ring in scheme:
        #"weight" is the array of weights, not a ring
        if ring == "weight":
            continue
        if ring not in weights:
            raise ValueError("Weights for {} undefined".format(ring))
        d[ring] = {}
        for dev in scheme[ring]:
            d[ring][dev] = weights[ring]
    return d

def build_weight_map(conf):
    '''build_weight_map -> dictionary

    Return is a hostname-keyed dictionary:
    d[hostname][ring][device]: weight
    '''
    wm = {}
    devmap = {}
    for scheme in conf['hosts']:
        if scheme not in conf['schemes']:
            raise ValueError("host scheme {} undefined".format(scheme))
        devmap[scheme] = build_device_weight_map(conf['schemes'][scheme])
        for host in conf['hosts'][scheme]:
            if isinstance(host,str): #simple hostname
                wm[host] = devmap[scheme]
            elif isinstance(host,dict): #override dictionary
                if len(host) != 1:
                    raise ValueError("Invalid host spec, parsed as {}".format(host))
                hostname = list(host)[0]
                wm[hostname] = devmap[scheme]
            else:
                raise ValueError("Unknown host spec {}".format(host))
    return wm

def rename_dict_key(d,oldname,newname):
    '''rename_dict_key(d,oldname,newname) -> replace key oldname in d

    This copies d[oldname] to d[newname] and then discards d[oldname],
    the effect of which is to essentially "rename" oldname to newname.

    Return is modified d

    '''
    d[newname] = d[oldname]
    _ = d.pop(oldname)
    return d

def rename_dict_keys(d,mapping):
    '''rename_dict_keys(d,mapping) -> apply mapping to keys of d

    Essentially, for every key of d, we look it up in mapping, and if
    found we rename it to the value found therein. E.g. if mapping is
    {"foo": "bar"}, we would copy d["foo"] to d["bar"] and discard
    d["foo"].

    Return is modified d.

    '''
    keys = list(d)
    for k in keys:
        if k in mapping:
            rename_dict_key(d,k,mapping[k])
    return d

def regularize_ring_names(schemes):
    '''regularize_ring_names(schemes) -> make rings match official names

    Our config file (following from the previous configuration scheme)
    uses human-friendly names (e.g. "ssds"); we want to convert them
    to what the rings are called (e.g. object-1 in this case).

    We do that for the ring names for each scheme, and then also for
    "weight" (which is the ring -> weight mapping)

    Return is schemes modified thus.

    '''
    mapping = { "ssds": "object-1",
                "objects": "object",
                "accounts": "account",
                "containers": "container" }
    for s in schemes:
        schemes[s] = rename_dict_keys(schemes[s],
                                      mapping)
        schemes[s]["weight"] = rename_dict_keys(schemes[s]["weight"],
                                                mapping)
    return schemes

def parse_config_file(path="/etc/swift/hosts.yaml"):
    '''parse_config_file(path) -> (weightmap,desired_state)

    parse config file at path and return a tuple with 2 elements:

    weightmap is a hostname-keyed dictionary of the partition scheme
    e.g. weightmap[hostname][devicename]=4000

    desired_state is likewise hostname keyed but device entries are
    WeightChange objects showing the value specified in the
    config file and whether the change should be made immediately
    (True) or gradually (False)
    e.g. desired_state[hostname][devicename]=(weight=4000,immediate=False)

    weightmap is what we'd expect the full weight of this device node
    in a host of this class (or partitioning scheme) to be;
    desired_state incorporates any overrides specified for the host or
    device.

    '''
    desired = {}
    with open(path,"r") as f:
        conf = yaml.safe_load(f)
        #turn friendly names (ssds) into ring names (object-1)
        conf['schemes'] = regularize_ring_names(conf['schemes'])
        for scheme in conf['hosts']:
            if scheme not in conf['schemes']:
                raise ValueError("host scheme {} undefined".format(scheme))
            desired = parse_scheme_hosts(conf['hosts'][scheme],
                                         conf['schemes'][scheme],
                                         desired)
    weightmap = build_weight_map(conf)
    return (weightmap,desired)

def run_and_check(args,allowed_returns=None):
    '''run_and_check(args,allowed_returns) -> run args, check return

    runs args (should be a list, passable to subprocess.Popen) using
    subprocess.communicate to capture stdout,stderr and return these
    to caller. If return value from subprocess is not in
    allowed_returns, raise a subprocess.CalledProcessError.

    If unset, allowed_returns defaults to [0]
    '''
    allowed_returns = allowed_returns or [0]
    s = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout,stderr = s.communicate()
    if s.returncode not in allowed_returns:
        print("'{}' returned {}.\nstdout: {}\nstderr: {}".format\
              (' '.join(args), s.returncode,
               stdout.decode(errors="replace"),
               stderr.decode(errors="replace")),
              file=sys.stderr)
        raise subprocess.CalledProcessError(s.returncode, args[0])
    return stdout,stderr

def cleanup_tmpdir():
    '''cleanup_tmpdir() - > remove tmpdir and contents'''
    if tmpdir is not None:
        shutil.rmtree(tmpdir)

def prepare_tmpdir(swiftdir="/etc/swift"):
    '''prepare_tmpdir() -> create (global) tmpdir, populate it

    If (global) tmpdir does not yet exist, make it and copy all
    of the rings from swiftdir into it.

    '''
    global tmpdir
    if tmpdir is None:
        tmpdir = tempfile.mkdtemp()
        for f in os.listdir(swiftdir):
            if f.endswith(".builder") or \
               f.endswith(".ring.gz"):
                shutil.copy("{}/{}".format(swiftdir,f),
                            "{}/{}".format(tmpdir,f))

def run_srb(ring,args,swiftdir="/etc/swift"):
    '''run_rb(ring,args,swiftdir) -> calls swift-ring-builder

    Runs swift-ring-builder on tmpdir copy of ring with args.
    Calls prepare_tmpdir to set up (global) tmpdir if necessary.

    '''
    prepare_tmpdir(swiftdir)
    tmppath = os.path.join(tmpdir,"{}.builder".format(ring))
    #For rebalance, allow return codes 0 (ring changed), 1 (ring unchanged)
    if args[0] == "rebalance":
        allowed_exits = [0,1]
    else:
        allowed_exits = [0]
    stdout,stderr = run_and_check(["/usr/bin/swift-ring-builder",
                                   tmppath]+args,
                                  allowed_exits)
    if len(stdout) != 0:
        debug(stdout.decode(errors="replace"))
    if len(stderr) != 0:
        debug(stderr.decode(errors="replace"))

def remove_host(host,current,swiftdir,dry_run=True):
    '''
    remove_host - remove host from all rings it's in
    '''
    verb = "Would" if dry_run else "Will"
    #work out which rings the host is in
    rings = []
    for dev in current:
        for ring in current[dev]:
            if ring not in rings:
                rings.append(ring)
    ip = forward_dns(host)
    rings.sort() # so debug order is stable
    debug("{} remove {}/{} from rings: {}".format(verb,
                                                  host,
                                                  ip,
                                                  ", ".join(rings)))
    if not dry_run:
        for ring in rings:
            run_srb(ring,["remove","--yes","--ip",ip],swiftdir)

def find_ip_region(ip):
    '''find_ip_regions(ip) -> returns the region the IP address is in.

    Currently only thanos uses regions, and it is codfw in 1 and eqiad
    in 2.

    '''
    if six.PY2:
        ipa = ipaddress.ip_address(unicode(ip)) # pylint: disable=E0602
    else:
        ipa = ipaddress.ip_address(ip)
    #eqiad is region 1
    if ipa in ipaddress.ip_network(u'10.64.0.0/12'):
        return 1
    #codfw is region 2
    if ipa in ipaddress.ip_network('10.192.0.0/12'):
        return 2
    #pontoon/cloud uses only region 1
    if ipa in ipaddress.ip_network('172.16.0.0/21'):
        return 1
    raise ValueError("Couldn't determine region for IP {}".format(ip))

def find_ip_zone(ip):
    '''find_ip_zone(ip) -> returns zone the IP address is in.

    Rather than use the textual approach from swift-add-machine,
    actually check which network the IP address is in.

    '''
    if six.PY2:
        ipa = ipaddress.ip_address(unicode(ip)) # pylint: disable=E0602
    else:
        ipa = ipaddress.ip_address(ip)
    # eqiad zones
    if ipa in ipaddress.ip_network(u'10.64.0.0/22'):
        return 1 #Row A
    if ipa in ipaddress.ip_network(u'10.64.16.0/22'):
        return 6 #Row B
    if ipa in ipaddress.ip_network(u'10.64.32.0/22'):
        return 3 #Row C
    if ipa in ipaddress.ip_network(u'10.64.48.0/22'):
        return 5 #Row D
    if ipa in ipaddress.ip_network(u'10.64.130.0/24')    \
       or ipa in ipaddress.ip_network(u'10.64.131.0/24') \
       or ipa in ipaddress.ip_network(u'10.64.132.0/24') \
       or ipa in ipaddress.ip_network(u'10.64.133.0/24') \
       or ipa in ipaddress.ip_network(u'10.64.152.0/24') \
       or ipa in ipaddress.ip_network(u'10.64.154.0/24') \
       or ipa in ipaddress.ip_network(u'10.64.156.0/24'):
        return 7 #Racks E1..E7
    if ipa in ipaddress.ip_network(u'10.64.134.0/24')    \
       or ipa in ipaddress.ip_network(u'10.64.135.0/24') \
       or ipa in ipaddress.ip_network(u'10.64.136.0/24') \
       or ipa in ipaddress.ip_network(u'10.64.137.0/24') \
       or ipa in ipaddress.ip_network(u'10.64.160.0/24') \
       or ipa in ipaddress.ip_network(u'10.64.162.0/24') \
       or ipa in ipaddress.ip_network(u'10.64.164.0/24'):
        return 8 #Racks F1..F7
    # codfw
    if ipa in ipaddress.ip_network(u'10.192.0.0/22')     \
       or ipa in ipaddress.ip_network(u'10.192.23.0/24') \
       or ipa in ipaddress.ip_network(u'10.192.9.0/24'):
        return 1 #Row A, Racks A2,A7
    if ipa in ipaddress.ip_network(u'10.192.16.0/22')    \
       or ipa in ipaddress.ip_network(u'10.192.15.0/24') \
       or ipa in ipaddress.ip_network(u'10.192.21.0/24'):
        return 2 #Row B, Rack B6,B7
    if ipa in ipaddress.ip_network(u'10.192.32.0/22')    \
       or ipa in ipaddress.ip_network(u'10.192.26.0/24') \
       or ipa in ipaddress.ip_network(u'10.192.31.0/24'):
        return 3 #Row C, Rack C7
    if ipa in ipaddress.ip_network(u'10.192.48.0/22')    \
       or ipa in ipaddress.ip_network(u'10.192.38.0/24') \
       or ipa in ipaddress.ip_network(u'10.192.39.0/24') \
       or ipa in ipaddress.ip_network(u'10.192.42.0/24'):
        return 4 #Row D, Rack D3,D4,D7
    # pontoon/cloud uses only zone 1
    if ipa in ipaddress.ip_network(u'172.16.0.0/21'):
        return 1
    raise ValueError("Couldn't determine zone for IP {}".format(ip))

def add_devs(ip,zone,region,ring,initial_port,increment,
             hostdevs,weights,swiftdir,dry_run=True):
    ''''add_devs -> add devices to the relevant ring

    Use weights to tell us the list of devices we want, and
    we add them in order (which is important if increment is True)

    returns the final port used
    '''
    port = initial_port
    devs = list(weights)
    devs.sort()
    for dev in devs:
        weight = hostdevs[dev].weight
        immediate = hostdevs[dev].immediate
        if not immediate:
            weight *= gradual
        debug("{} in {} weight {} port {}".format(dev,
                                                  ring,
                                                  weight,
                                                  port))
        if not dry_run:
            run_srb(ring,["add","--region",str(region),
                          "--zone",str(zone),
                          "--ip",ip,"--port",str(port),
                          "--weight",str(weight),
                          "--device",dev],swiftdir)
        if increment:
            port += 1
    return port

def check_immediate_host_addition(desired):
    '''check_immediate_host_addition(desired) -> Bool

    Check if any of the devices are flagged as immediate - if so,
    return True (i.e. add this host even if immediate_only was set);
    otherwise return False.

    We don't want to be having to try and add partial hosts, so make
    the go/no-go decision at the host level.

    '''
    for wc in desired.values():
        if wc.immediate:
            return True
    return False

def add_host(host,desired,weightmap,swiftdir,dry_run=True):
    '''
    add_host - add host to rings (with suitable weighting)
    '''
    verb = "Would" if dry_run else "Will"
    ip = forward_dns(host)
    zone = find_ip_zone(ip)
    if "thanos-" in host:
        region = find_ip_region(ip)
    else:
        region = 1
    debug("{} add host {}({}) in zone {} (region {}) as follows:".format(verb,
                                                                         host,
                                                                         ip,
                                                                         zone,
                                                                         region))
    #We need to handle rings and devices in order because of port numbering
    #for object and object-1/ssd rings
    port = 6010
    if "object" in weightmap:
        port = add_devs(ip,zone,region,"object",port,True,
                        desired,weightmap["object"],
                        swiftdir,dry_run)
    if "object-1" in weightmap:
        port = add_devs(ip,zone,region,"object-1",port,True,
                        desired,weightmap["object-1"],
                        swiftdir,dry_run)
    #accounts and container always use the same port
    if "account" in weightmap:
        add_devs(ip,zone,region,"account",6002,False,
                 desired,weightmap["account"],swiftdir,dry_run)
    if "container" in weightmap:
        add_devs(ip,zone,region,"container",6001,False,
                 desired,weightmap["container"],swiftdir,dry_run)

def update_weight(current,desired,weightmap):
    '''update_weight(current,desired,weightmap) -> weight

    This function used where changes are being made gradually -
    calculate the new weight based on moving a proportion "gradual" of
    the full weight (from weightmap) towards desired weight.

    '''
    #if only small change remaining, do all of it
    if abs(desired-current) < gradual * weightmap:
        return desired
    increment = gradual * weightmap
    if desired > current: # pylint: disable=R1705
        return current + increment
    else:
        return current - increment

def compare_weights(host,current,desired,weightmap,immediate_only,
                    swiftdir,dry_run=True):

    '''compare_weights(host,current,desired,weightmap,immediate_only,dry_run)

    Compare current and desired weights for every device on host, and
    adjust as needed, taking into account whether the change is
    immediate or not.

    Return count of adjustments
    '''
    verb = "Would" if dry_run else "Will"
    ip = forward_dns(host)
    changed = 0

    for device in current:
        #structure is d[hostname][devicename]=(weight=x,immediate=y)
        dw = desired[device].weight
        immediate = desired[device].immediate
        if immediate_only and not immediate:
            continue
        #structure is d[hostname]={devicename:{"ring":weight}}
        #we are passed d["hostname"]
        for ring in current[device]:
            wm = weightmap[ring][device]
            cw = current[device][ring]
            if cw != dw:
                if immediate:
                    weight = dw
                else:
                    weight = update_weight(cw,dw,wm)
                debug("{} set weight {}/{} in {} to {}".format(verb,
                                                               host,
                                                               device,
                                                               ring,
                                                               weight))
                changed += 1
                if not dry_run:
                    run_srb(ring,["set_weight",
                                  "{}/{}".format(ip,device),
                                  str(weight)],swiftdir)
    return changed

def rebalance():
    '''rebalance -> rebalance all rings in tmpdir

    '''
    if tmpdir is not None:
        for ring in os.listdir(tmpdir):
            if fnmatch.fnmatch(ring,"*.builder"):
                ringname = ring.replace('.builder','')
                run_srb(ringname,["rebalance","-f"])

def deploy_rings(outdir):
    '''deploy_rings(outdir) -> make outdir/new_rings.tar.bz2

    We do this by building the tarball in tmpdir and then using
    rename() to copy it into outdir; this is an atomic operation, so
    anything reading the tarball will get either all the old rings or
    all the new rings.

    '''
    if tmpdir is None:
        raise ValueError("Cannot deploy without changed rings")
    tarname = "new_rings.tar.bz2"
    with tarfile.open("{}/{}".format(tmpdir,tarname),"w:bz2") as t:
        for f in os.listdir(tmpdir):
            if f.endswith(".builder") or \
               f.endswith(".ring.gz"):
                t.add("{}/{}".format(tmpdir,f),
                      arcname=f)
    os.rename("{}/{}".format(tmpdir,tarname),
              "{}/{}".format(outdir,tarname))

def compare_states(desired,current,weightmap,immediate_only,
                   swiftdir,dry_run=True):
    '''compare_states(desired,current,weightmap,immediate_only,swiftdir,dry_run)

    Find the differences between desired and current state, if dry_run
    is False, make those changes to the rings.

    If immediate_only is True, then only make changes marked as immediate.

    '''
    removed = 0
    added = 0
    changed = 0
    #Get the union of hosts (i.e. declared in either desired or current)
    hosts = set(desired.keys()) | set(current.keys())
    for host in hosts:
        if host not in desired:
            if not immediate_only:
                remove_host(host,current[host],swiftdir,dry_run)
                removed += 1
        elif host not in current:
            if immediate_only:
                if not check_immediate_host_addition(desired[host]):
                    debug("skipping addition of {} as not immediate".format\
                          (host))
                    continue
            add_host(host,desired[host],weightmap[host],
                     swiftdir,dry_run)
            added +=1
        else:
            changed += compare_weights(host,current[host],
                                       desired[host],
                                       weightmap[host],
                                       immediate_only,swiftdir,dry_run)
    if dry_run:
        verb = "Would a"
    else:
        verb = "A"
    msg = ("{}dd {} host(s), remove {}, change {} weights".format\
           (verb,added,removed,changed))
    logging.info(msg)
    if not dry_run:
        rebalance()

def cli_err(msg,parser):
    '''cli_err(msg,parser) -> msg and usage to stderr, exit 1

    Print msg to stderr, followed by parser.print_help, then exit 1
    '''
    print(msg, file=sys.stderr)
    parser.print_help(file=sys.stderr)
    sys.exit(1)

def setup_logging(syslog=False,verbose=False):
    '''setup_logging() -> set up logging to stdout or syslog

    if syslog set, log to syslog not stdout; log level is set to INFO
    unless verbose is True, in which case DEBUG
    '''
    if verbose:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
    if syslog:
        handler = logging.handlers.SysLogHandler('/dev/log')
        handler.formatter = logging.Formatter('%(asctime)s: %(levelname)s: %(message)s')
        logger.addHandler(handler)
    else:
        logging.basicConfig(format='%(message)s',stream=sys.stdout)

def parse_cli():
    '''parse_cli() -> parse CLI arguments, sanity checks

    Parses CLI arguments; does basic sanity checks that options chosen
    are not incompatible.
    Returns the output of argparse.ArgumentParser.parse_args()

    '''
    parser = argparse.ArgumentParser(description='Swift Ring Manager')
    parser.add_argument('--doit',
                        help="Update ring files and deploy",
                        action="store_true")
    parser.add_argument('--immediate-only',
                        help="Only make changes flagged as immediate",
                        action="store_true")
    parser.add_argument('--update-tmp-rings',
                        help="Only update temporary copies of ring files",
                        action="store_true")
    parser.add_argument('-v','--verbose',
                        help="emit more debugging information",
                        action="store_true")
    parser.add_argument('-c','--conf-file',
                        help="path to configuration file",
                        default="/etc/swift/hosts.yaml")
    parser.add_argument('--ring-dir',
                        help="directory containing ring builder files",
                        default="/etc/swift")
    #defaults to whatever ring-dir is set to
    parser.add_argument('-o','--out-dir',
                        help="directory to put new rings into")
    parser.add_argument('--skip-dispersion-check',
                        help="don't check dispersion OK before making changes",
                        action="store_true")
    parser.add_argument('--skip-replication-check',
                        help="don't check replication OK before making changes",
                        action="store_true")
    parser.add_argument('-s','--syslog',
                        help="Log to syslog rather than stdout",
                        action="store_true")
    parser.add_argument('--host-file-path',
                        help="fake hosts list file path")
    parser.add_argument('--generate-host-file',
                        help="generate a hosts file for future testing use",
                        action="store_true")
    parser.add_argument('--rebalance',
                        help="rebalance rings even if no other changes made",
                        action="store_true")
    args = parser.parse_args()
    if args.host_file_path is not None and args.doit:
        cli_err("Error: Musn't use --host-file-path except in dry-run mode",
                parser)
    if args.host_file_path is None and args.generate_host_file:
        cli_err("Error: must specify host-file-path for --generate-host-file",
                parser)
    if args.out_dir is None:
        args.out_dir = args.ring_dir
    else:
        if not os.path.isdir(args.out_dir):
            cli_err("Error: output directory must exist",
                    parser)
    #--doit with --update-tmp-rings is probably not want the user wanted
    #but we can proceed anyhow
    if args.update_tmp_rings:
        if args.doit:
            print("--update-tmp-rings => deployed ring files don't change",
                  file=sys.stderr)
        else: #set this internally, so we are not in dry-run mode
              #and will build new ring files in a tmpdir
            args.doit = True
    return args

def main():
    '''main() -> main program logic'''
    #Parse options (e.g. verbose, dry-run, ...)
    args = parse_cli()
    setup_logging(args.syslog, args.verbose)
    #Check dispersion all 100% and replication has caught up OK
    #If not, only consider "immediate" changes
    repl_ok = True
    if not args.skip_replication_check:
        repl_ok = check_replication()
    disp_ok = True
    if not args.skip_dispersion_check:
        disp_ok = check_all_dispersions()
    if not disp_ok or not repl_ok:
        immediate_only = True
    else:
        immediate_only = args.immediate_only
    #Parse our YAML config file of what we think the world should look like
    weightmap,desired = parse_config_file(args.conf_file)
    if args.generate_host_file:
        write_host_file(desired,args.host_file_path)
        sys.exit(0)
    if args.host_file_path is not None:
        read_host_file(args.host_file_path)
    if args.doit:
        #install a handler to tidy up tmpdir on exit
        if not args.update_tmp_rings:
            atexit.register(cleanup_tmpdir)
        try:
            with lock_path(args.ring_dir):
                #Use RingBuilder.load to load the rings
                rings = load_rings(args.ring_dir)
                #Calculate and apply change
                compare_states(desired,rings,weightmap,
                               immediate_only,args.ring_dir,False)
                if args.rebalance and tmpdir is None:
                    debug("Rebalancing anyway as requested")
                    prepare_tmpdir(args.ring_dir)
                    rebalance()
                if args.update_tmp_rings:
                    if tmpdir is not None:
                        print("Updated rings are in {}".format(tmpdir))
                else:
                    if tmpdir is not None:
                        deploy_rings(args.out_dir)
        except exceptions.LockTimeout:
            print("Unable to get lock on {}".format(args.ring_dir),
                  file=sys.stderr)
            sys.exit(1)
    else: #don't need to lock anything
        rings = load_rings(args.ring_dir)
        #Calculate change; don't apply
        compare_states(desired,rings,weightmap,
                       immediate_only,args.ring_dir,True)

if __name__ == "__main__":
    main()
